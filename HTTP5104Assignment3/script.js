//Scroll to sections
window.onload = function () {
    function scrollPage(nameattr) {
        var anchor = $("a[id='" + nameattr + "']");
        $('html,body').animate({ scrollTop: anchor.offset().top }, 'slow');
    }

    $("#toProjects").click(function () {
        scrollPage('projects');
    })
    
    $("#toProjectsNav").click(function(){
        scrollPage('projects');
    })
    
    $("#toContactNav").click(function () {
        scrollPage('contact');
    })

    //Trident Modal
    var tridentModal = document.getElementById("tridentModal");
    var tridentBtn = document.getElementById("tridentProject");
    var tridentSpan = document.getElementById("closeTridentModal");

    //Tollo Modal
    var tolloModal = document.getElementById("tolloModal");
    var tolloBtn = document.getElementById("tolloProject");
    var tolloSpan = document.getElementById("closeTolloModal");

    //ESL Modal
    var ESLModal = document.getElementById("esportsloungeModal");
    var ESLBtn = document.getElementById("esportsloungeProject");
    var ESLSpan = document.getElementById("closeEsportsLoungeModal");
    
    //Wild Coinflip Modal
    var CoinflipModal = document.getElementById("coinflipModal");
    var CoinflipBtn = document.getElementById("coinflipProject");
    var CoinflipSpan = document.getElementById("closeCoinflipModal");
    
    //XML Chatroom Modal
    var ChatroomModal = document.getElementById("chatroomModal");
    var ChatroomBtn = document.getElementById("chatroomProject");
    var ChatroomSpan = document.getElementById("closeChatroomModal");
    
    //Portfolio Modal
    var PortfolioModal = document.getElementById("portfolioModal");
    var PortfolioBtn = document.getElementById("portfolioProject");
    var PortfolioSpan = document.getElementById("closePortfolioModal");
    
    //E7 Armory Modal
    var e7Modal = document.getElementById("e7Modal");
    var e7Btn = document.getElementById("e7Project");
    var e7Span = document.getElementById("closee7Modal");
    
    //Dinner Dash Modal
    var ddModal = document.getElementById("ddModal");
    var ddBtn = document.getElementById("ddProject");
    var ddSpan = document.getElementById("closeddModal");

    //Hi Spently! Modal
    var hsModal = document.getElementById("hsModal");
    var hsBtn = document.getElementById("hsProject");
    var hsSpan = document.getElementById("closehsModal");

    //Infinity Atmosphere Modal
    var iaModal = document.getElementById("iaModal");
    var iaBtn = document.getElementById("iaProject");
    var iaSpan = document.getElementById("closeiaModal");

    const modals = [tridentModal, tolloModal, ESLModal, CoinflipModal, ChatroomModal, PortfolioModal, e7Modal, ddModal, hsModal, iaModal];
    const buttons = [tridentBtn, tolloBtn, ESLBtn, CoinflipBtn, ChatroomBtn, PortfolioBtn, e7Btn, ddBtn, hsBtn, iaBtn];
    const spans = [tridentSpan, tolloSpan, ESLSpan, CoinflipSpan, ChatroomSpan, PortfolioSpan, e7Span, ddSpan, hsSpan, iaSpan];

    const showModals = () =>{
        for(let i = 0; i < buttons.length; i++)
        {
            buttons[i].onclick = () =>{
                modals[i].style.display = "block";
            }
        }
    }

    showModals();

    window.onclick = (event) => {
        
        for(let i = 0; i < modals.length; i++){
            if(event.target == modals[i] || event.target == spans[i])
            {
                modals[i].style.display = "none";
            }
        }
    }

    //Responsive nav
    var hamburgerMenu = document.getElementById("hamburger");
    var navigationMenu = document.getElementById("menuLinks");

    hamburgerMenu.onclick = () => {
        if (navigationMenu.className != "showMenu") {
            navigationMenu.className = "showMenu";
        }
        else {
            navigationMenu.className = "";
        }
    }

}

// About section fade
$(document).ready(function(){
    $(window).scroll(function(){
        $('.fadeThis').each(function(){
            var bottomOfObject = $(this).offset().top;
            var bottomOfWindow = $(window).scrollTop() + $(window).height();

            if(bottomOfWindow > bottomOfObject)
            {
                $(this).animate({'opacity':'1'}, 500);
            }
        });
    });

    
});
